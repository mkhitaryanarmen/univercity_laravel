<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


/* FACULTY CRUD HERE */

Route::get('/faculties', 'FacultyController@getFaculties');

Route::post('/faculty', 'FacultyController@postFaculty');

Route::put('/faculty/{id}', 'FacultyController@putFaculty');

Route::delete('/faculty/{id}', 'FacultyController@deleteFaculty');



/* GROUP CRUD HERE */

Route::get('/groups/{id}', 'GroupController@getGroups');

Route::post('/group', 'GroupController@postGroup');

Route::put('/group/{id}', 'GroupController@putGroup');

Route::delete('/group/{id}', 'GroupController@deleteGroup');


/* STUDENTS CRUD HERE */

Route::get('/students', 'StudentController@getStudents');

Route::post('/student', 'StudentController@postStudent');

Route::put('/student/{id}', 'StudentController@putStudent');

Route::delete('/student/{id}', 'StudentController@deleteStudent');





