<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function postStudent(Request $request)
    {

    }

    public function getStudents()
    {
        $students = Faculty::join('groups', 'faculties.id', '=', 'groups.faculties_id')
            ->join('students', 'groups.id', '=', 'students.groups_id')
            ->select('students.*', 'faculties.name', 'groups.group_name')
            ->get();

        return response()->json(['students' => $students], 200);
    }

    public function putStudent(Request $request, $id)
    {

    }

    public function deleteStudent($id)
    {

    }
}
