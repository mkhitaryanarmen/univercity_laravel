<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function postFaculty(Request $request)
    {
        $faculty = new Faculty();
        $faculty->name = $request->input('name');
        $faculty->save();
        return response()->json(['faculty' => $faculty], 200);
    }

    public function getFaculties()
    {
        $faculties = Faculty::all();
        return response()->json(['faculties' => $faculties], 200);
    }

    public function putFaculty(Request $request, $id)
    {
        $faculty = Faculty::find($id);

        if (!$faculty) {
            return response()->json(['msg' => 'Faculty do not found!'], 404);
        }
        $faculty->name = $request->input('name');
        $faculty->save();
        return response()->json(['faculty' => $faculty], 200);
    }

    public function deleteFaculty($id)
    {
        $faculty = Faculty::find($id);
        $faculty->delete();
        return response()->json(['msg' => 'Faculty deleted!']);
    }
}
