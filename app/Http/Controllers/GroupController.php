<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    public function postGroup(Request $request)
    {

    }

    public function getGroups($id)
    {
//        $groups = DB::table('groups')->where('group_name', '=', $id)->get();
        $groups = Group::find($id);
        if (!$groups) {
            return response()->json(['msg' => 'Faculty do not found!'], 404);
        }

        return response()->json(['groups' => $groups], 200);
    }

    public function putGroup(Request $request, $id)
    {

    }

    public function deleteGroup($id)
    {

    }
}
